<?php
namespace Alsodev\Crudadmin\Providers;

use Illuminate\Support\ServiceProvider;

class AlsodevCrudProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // route
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        // public
        $this->publishes([
            __DIR__.'/../config' => config_path('alsodev_crudadmin'),
            __DIR__.'/../public' => public_path('alsodev/crud'),
            __DIR__.'/../Models' => app_path('Models'),
        ]);
        // views
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'AlsodevCrudadmin');
        // migration
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
