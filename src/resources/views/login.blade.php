<html>
    <head>
        <title>
            Login page
        </title>
        <style>
            html{
                box-sizing: border-box;
                font-family: 'Roboto', sans-serif;
            }
            *,*::before,*::after{
                box-sizing: inherit;
                margin: 0px;
            }
            .wrapper_login{
                display: flex;
                flex-wrap: wrap;
            }
            .img_left{
                width: 50%;
            }

            .img_left  .imgleft{
                width: 100%;
                height: 100vh;
                object-fit: cover;
                object-position: center;
            }
            .part_right{
                width: 50%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .form_enter{
                width: 100%;
                max-width: 320px;
                padding: 15px;
                background: gainsboro;
                border-radius: 10px;
                -webkit-box-shadow: 0px 5px 10px 2px rgba(34, 60, 80, 0.2);
                -moz-box-shadow: 0px 5px 10px 2px rgba(34, 60, 80, 0.2);
                box-shadow: 0px 5px 10px 2px rgba(34, 60, 80, 0.2);
            }
            .form_w100{
                width: 100%;
                color: #333;
                font-family: Poppins;
                font-size: 18px;
                font-style: normal;
                font-weight: 400;
                line-height: normal;
            }
            .forminput{
                width: 100%;
                padding: 18px 26px;
                flex-direction: column;
                justify-content: center;
                align-items: flex-start;
                gap: 10px;
                border-radius: 30px;
                border: 1px solid #EEE;
                background: #FFF;
            }
            .btn_submit{
                display: flex;
                width: 100%;
                margin-top: 10px;
                padding: 18px 26px;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                gap: 10px;
                border-radius: 30px;
                background: #0575E6;
                border: 1px solid #EEE;
                color: white;
                cursor: pointer;
            }
            .form_w100.erros{
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="wrapper_login">
            <div class="img_left">
                <img src="{{ asset('alsodev/crud/img/png/loginpage.png') }}" class="imgleft">
            </div>
            <div class="part_right">
                <div class="form_enter">
                    <form method="post" action="{{ route('login.post') }}">
                        <div class="form_w100">
                            email
                        </div>
                        <div class="form_w100">
                            <input type="email" class="forminput" name="email" required>
                        </div>
                        <div class="form_w100">
                            password
                        </div>
                        <div class="form_w100">
                            <input type="password" class="forminput" name="password" required>
                        </div>
                        <div class="form_w100 erros">
                            @if (session('error'))
                                <div class="alert alert-success">
                                    {{ session('error') }}
                                </div>
                            @endif
                        </div>
                        <input type="submit" class="btn_submit" value="login">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
