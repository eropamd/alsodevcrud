@extends('AlsodevCrudadmin::layout.app',['user'=>$user])

@section('title', 'Создать запись')

@section('content')
    <div class="wrapper_right">
        <div class="listTitle">
            {{ $confDatas['createTitle']  }}
        </div>

        <div class="card_w100">
            <form action="{{ route('crud.store',['namecrud'=>$name]) }}" method="post">
                @foreach($confDatas['createColumn'] as $key=>$value)
                    <div class="wraper_form_input">
                        <div class="title_w50">
                            {{ $value['title'] }}
                        </div>
                        <div class="value_w50">
                            <?php
                                $input=$builder->builderBoss($value['type']);
                                $input->setName($key);
                            ?>
                            {!!$input->getInput() !!}
                        </div>
                    </div>
                @endforeach
                @csrf
                <input type="submit">

            </form>
        </div>




    </div>
@endsection
