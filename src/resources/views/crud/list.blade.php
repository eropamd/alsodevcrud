@extends('AlsodevCrudadmin::layout.app',['user'=>$user])

@section('title', 'List')

@section('content')
    <div class="wrapper_right">
        <div class="listTitle">
            {{ $confDatas['listTitle']  }}
        </div>
        <div class="newRecord">
            <a class="btnNewRecord" href="{{ route('crud.create',['namecrud'=>$name]) }}">Создать запись</a>
        </div>

        <div class="card_w100">
            <table {!!  ($tableList->getClassName()!==""? 'class="'.$tableList->getClassName().'"':'' )  !!}>
                {!!  $tableList->strHtmlHead() !!}
                {!!  $tableList->strHtmlBody() !!}
            </table>
        </div>
        <input type="hidden" id="name_param" value="{{$name}}">
    </div>

    <script>
        // Полдучаем все кнопки удаления
        var deleteBtns = document.getElementsByClassName("trash-record")
        // Вешаем события
        for (var i = deleteBtns.length; i--;)  deleteBtns[i].addEventListener('click', checkMulti)
        // Функция удаление
        function checkMulti() {
            console.log(this.getAttribute('data-id'))
        }

    </script>

@endsection
