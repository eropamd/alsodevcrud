<html>
    <head>
        <title>
            Панель управления
        </title>

        <style>
            html{
                box-sizing: border-box;
                font-family: 'Roboto', sans-serif;
                background: #E2E8F0;
            }
            *,*::before,*::after{
                box-sizing: inherit;
                margin: 0px;
            }
            .wrapper_head{
                display: flex;
                flex-wrap: wrap;
                height: 64px;
                justify-content: space-between;
                align-content: center;
                background: #FFF;
            }
            .wrapper_head .left_head{
                width: 240px;
                padding-left: 24px;
                display: flex;
                flex-wrap: wrap;
                align-items: center;
            }
            .wrapper_head .right_head{
                display: flex;
                flex-wrap: wrap;
                align-items: center;
            }
            .wrapper_head .btn_exit{
                background-color: #44c767;
                border-radius: 28px;
                border: 1px solid #18ab29;
                display: inline-block;
                cursor: pointer;
                color: #ffffff;
                font-family: Arial;
                font-size: 16px;
                padding: 16px 31px;
                text-decoration: none;
                text-shadow: 0px 1px 0px #2f6627;
                margin-left: 10px;
                padding-bottom: 2px;
                padding-top: 3px;
                margin-right: 10px;
            }
            .wrapper_head .btn_exit:hover {
                background-color:#5cbf2a;
            }
            .wrapper_head .btn_exit:active {
                position:relative;
                top:1px;
            }

            .wrapper_body{
                display: flex;
                flex-wrap: wrap;
            }
            .wrapper_body .left_body{
                width: 240px;
                margin-top: 20px;
            }
            .wrapper_body .left_body .item_link{
                padding-left: 40px;
            }

            .wrapper_body .left_body .item_link a{
                text-decoration: none;
                color: #64748B;
                font-family: Nunito Sans;
                font-size: 18px;
                font-weight: 400;
            }


            .wrapper_body .right_body{
                width: calc(100% - 240px);
                padding-top: 20px;
            }
            .wrapper_right{
                width: 100%;
                display: flex;
                flex-wrap: wrap;
            }
            .wrapper_right .listTitle{
                color: #64748B;
                font-size: 24px;
                font-weight: 400;
                font-family: Nunito Sans;
                text-decoration: none;
                width: 100%;
                margin-bottom: 20px;
            }
            .wrapper_right  .newRecord{
                width: 100%;
                margin-bottom: 20px;
            }
            .wrapper_right  .btnNewRecord{
                background: green;
                color: white;
                padding: 10px;
                width: 200px;
                border-radius: 10px;
                text-align: center;
                position: relative;
                margin-bottom: 10px;
                text-decoration: none;
            }
            .wrapper_right .card_w100{
                background: white;
                width: 95%;
                min-height: 200px;
                padding: 20px 10px 10px 15px;

            }

            /**
            Table start
             */
            .tablelist{
                width: 100%;
                border-collapse: collapse;
            }
            .tablelist thead{
                background: #CBD5E1;
                color: #64748B;
                font-family: Nunito Sans;
                font-size: 12px;
                font-weight: 800;
                text-align: left;
            }
            .tablelist thead th{
                padding: 8px;
            }
            .tablelist tbody tr{
                height: 38px;
                padding: 8px;
                background: #F1F5F9;
                font-family: 'Nunito Sans';
                font-size: 14px;
                color: #64748B;
            }
            .tablelist tbody tr{
                border-bottom-width: 2px; /* Толщина линии внизу */
                border-bottom-style: solid; /* Стиль линии внизу */
                border-bottom-color: white; /* Цвет линии внизу */
            }
            .tablelist tbody tr:hover{
                background: #3e454b;
                color: white;
                font-weight: bold;
            }


            /**
             Форма
             */
            .wraper_form_input{
                display: flex;
                flex-wrap: wrap;
                width: 100%;
                align-items: center;
                justify-content: flex-start;
                margin-bottom: 10px;
            }
            .wraper_form_input .title_w50{
                width: 250px;
                color:  #64748B;
                /* Body Regular */
                font-family: Nunito Sans;
                font-size: 14px;
                font-style: normal;
                font-weight: 400;
                line-height: 20px; /* 142.857% */
            }
            .wraper_form_input .value_w50{
               width: calc(100% - 250px);
            }
            .wraper_form_input .value_w50 input{
                width: 100%;
                display: flex;
                padding: 20px 32px 20px 24px;
                align-items: center;
                gap: 226px;
                align-self: stretch;
                background: #FFF;
                border-radius: 4px;
                border: 1px solid #CBD5E1;
            }
        </style>
    </head>
    <body>

        <div class="wrapper_head">
            <div class="left_head">
                CrudAdmin
            </div>
            <div class="right_head">
                {{$user->email}}
                <form method="post" action="{{route('logout.post')}}">
                    <input type="submit" value="Exit" class="btn_exit">
                    @csrf
                </form>
            </div>
        </div>
        <div class="wrapper_body">
            <div class="left_body">
                <?php
                $getUserMenus= \Alsodev\Crudadmin\Services\UseConfig::getUserMenu();
                ?>
                <div class="wrapper_menus">
                    @foreach($getUserMenus as $key=>$item)
                        <div class="item_link">
                            <a href="{{ route('crud.list',['namecrud'=>$key]) }}">{{$item['title']}}</a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="right_body">
                @yield('content')
            </div>
        </div>
    </body>
</html>
