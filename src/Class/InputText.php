<?php

namespace Alsodev\Crudadmin\Class;

class InputText implements HtmlInput
{
    protected $inputClassName="";
    protected $name="";
    public function setClass(string $className): void
    {
        $this->inputClassName=$className;
    }
    public function setName(string $name):void
    {
        $this->name=$name;
    }
    public function getInput(): string
    {
        $inputText= "<input type='text'";
        $inputText.= ($this->inputClassName!==''?"class ='".$this->inputClassName."'":"");
        $inputText.= ($this->name!==''?"name ='".$this->name."'":"");
        $inputText.= ">";
        return $inputText;
    }
}
