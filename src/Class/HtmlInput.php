<?php

namespace Alsodev\Crudadmin\Class;

interface HtmlInput
{
    public function setClass(string $className):void;

    public function setName(string $name):void;

    public function getInput():string;
}
