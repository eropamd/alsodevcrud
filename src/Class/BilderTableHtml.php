<?php

namespace Alsodev\Crudadmin\Class;



class BilderTableHtml
{
    protected  $arrayColumns=[];
    protected  $arraySelect=[];
    protected  $strModel='';
    protected  $classNameTable='';
    protected  $name='';

    /**
     * Задаем сразу значение которые нужны чтобы отработала форма
     * @param array $columns
     * @param String $nameModel
     * @return void
     */
    public function setTableData(array $columns,String $nameModel,String $classname="",String $name=""):void
    {
        $this->strModel=$nameModel;
        $this->classNameTable=$classname;
        $this->name=$name;
        foreach ($columns as $key=>$value){
            $this->arrayColumns[]=$value['title'];
            $this->arraySelect[]=$key;
        }
    }


    /**
     * Выводим шапку
     * @return string
     */
    public function strHtmlHead():string
    {
        $strReturn="<thead><tr>";
        foreach ($this->arrayColumns as $item){
            $strReturn.="<th>".$item."</th>";
        }
        $strReturn.="<th></th>";
        $strReturn.="</tr></thead>";
        return $strReturn;
    }

    /**
     * Получаем массив из запроса
     * @return mixed
     */
    protected function getDatas():array
    {
        //dd($this->strModel);
        $model = app($this->strModel);
        $dataDb=$model->select( $this->arraySelect)->get()->toArray();
        return $dataDb;
    }

    /**
     * Выводи html код тело запроса
     * @return string
     */
    public function strHtmlBody():string
    {
        $idRecord=0;
        $strBodyTable="<tbody>";
        $datas=$this->getDatas();
        foreach ($datas as $data){
            $strBodyTable .="<tr>";
            foreach ($this->arraySelect as $itemSelect){
                if($itemSelect==="id")
                    $idRecord=$data[$itemSelect];
                $strBodyTable .="<td>".$data[$itemSelect]."</td>";
            }
            $strBodyTable .="<td>";
            $strBodyTable .="<img src='".asset('alsodev/crud/img/png/pencil-alt.png')."'>";
            $strBodyTable .="<img data-id='".$idRecord."' class='trash-record' src='".asset('alsodev/crud/img/png/trash.png')."'>";
            $strBodyTable .="</td>";
            $strBodyTable .="</tr>";
        }
        $strBodyTable .="</tbody>";
        return $strBodyTable;
    }

    /**
     * Если нужно вывести класс который будет в таблице
     * @return String
     */
    public function getClassName():String
    {
        return $this->classNameTable;
    }


}
