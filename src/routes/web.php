<?php


use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\Auth\AuthenticatedSessionController;
use Alsodev\Crudadmin\Http\Controllers\LoginController;
use Alsodev\Crudadmin\Http\Controllers\CrudController;


/**
 * Admin route
 */
Route::middleware(['web','auth'])->group(function () {
    //Url prefix
    $prefix= config('alsodev_crudadmin.options.admin_puth');
    //logout system crud
    Route::post($prefix.'/logout', [LoginController::class, 'logout'])
        ->name('logout.post');
    Route::get($prefix, [LoginController::class, 'home'])
        ->name('crud.home');
    Route::get($prefix.'/{namecrud}/list', [CrudController::class, 'index'])
        ->name('crud.list');
    Route::get($prefix.'/{namecrud}/create', [CrudController::class, 'create'])
        ->name('crud.create');
    Route::post($prefix.'/{namecrud}/store', [CrudController::class, 'store'])
        ->name('crud.store');
});


// Login route
Route::middleware('web')->group(function () {
    //Url prefix
    $prefix= config('alsodev_crudadmin.options.admin_puth');
    // store login post data
    Route::post($prefix.'/login', [LoginController::class, 'store'])
        ->name('login.post');
    // show page login
    Route::get('/'.$prefix.'/login', [LoginController::class, 'login'])
        ->name('login.page');
    Route::get('/'.$prefix.'/login', [LoginController::class, 'login'])
        ->name('login');
});


