<?php
return [
    'models'=>'App\\Models\\User',
    'listTitle'=>"Список пользователей",
    'listColumn' => [
        'id'=>[
            'title'=>'ID пользователя',
            'type'=>'text',
        ],
        'name'=>[
            'title'=>'Ф.И.О. пользователя',
            'type'=>'text',
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'text',
        ]
    ],
    'createTitle'=>"Создать пользователя",
    'createColumn' => [
        'name'=>[
            'title'=>'Ф.И.О. пользователя',
            'type'=>'text',
            'required'=>1,
            'class'=>"form_input",
        ],
        'email'=>[
            'title'=>'email',
            'type'=>'text',
            'required'=>1,
            'class'=>"form_input",
        ],
        'password'=>[
            'title'=>'password',
            'type'=>'text',
            'required'=>1,
            'class'=>"",
        ],
    ],

];
