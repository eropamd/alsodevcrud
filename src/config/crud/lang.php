<?php
return [
    'models'=>'App\\Models\\LangData',
    'listTitle'=>"Список языков",
    'listColumn' => [
        'id'=>[
            'title'=>'ID',
            'type'=>'text',
        ],
        'short_name'=>[
            'title'=>'Короткое название',
            'type'=>'text',
        ],
        'full_name'=>[
            'title'=>'Полное название',
            'type'=>'text',
        ]
    ],
    'createTitle'=>"Создать язык",
    'createColumn' => [
        'short_name'=>[
            'title'=>'Короткое название',
            'type'=>'text',
            'required'=>1,
            'class'=>"form_input",
        ],
        'full_name'=>[
            'title'=>'Полное название',
            'type'=>'text',
            'required'=>1,
            'class'=>"form_input",
        ],
    ],
];
