<?php

namespace Alsodev\Crudadmin\Http\Controllers;

use Alsodev\Crudadmin\Class\BilderTableHtml;
use Alsodev\Crudadmin\Class\BossBuilder;
use App\Http\Controllers\Controller;
use App\Models\LogCrud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CrudController extends Controller
{

    private function getModel($name){
        // Подгружаем что должно вывести
        $confDatas=config('alsodev_crudadmin.crud.'.$name);
        // ПРоверка есть ли конфигурационный файл
        if(is_null($confDatas))
            abort(404);
        return $confDatas;
    }

    /**
     * Выводи формы в виде списка
     * @param String $name
     * @return View
     */
    public function index(String $name):View
    {
        // Получаем пользователя текущего
        $user=Auth::user();
        $confDatas=$this->getModel($name);
        // Создаем экземпляр класса таблицы
        $tableList=new BilderTableHtml();
        // задаем шапку
        $tableList->setTableData($confDatas['listColumn'],$confDatas['models'],'tablelist');
        // Показываем в форме
        return view("AlsodevCrudadmin::crud.list",['user'=> $user,
            'confDatas'=>$confDatas,
            'tableList'=>$tableList,
            'name'=>$name]);
    }


    /**
     * Вывод формы создания
     * @param String $name
     * @return View
     */
    public function create(String $name):View
    {
        // Получаем пользователя текущего
        $user=Auth::user();
        $confDatas=$this->getModel($name);
        // Создаем экземпляр класса таблицы
        $tableList=new BilderTableHtml();
        // задаем шапку
        $tableList->setTableData($confDatas['listColumn'],$confDatas['models'],'tablelist');
        $builder=new BossBuilder();
        // Показываем в форме
        return view("AlsodevCrudadmin::crud.create",['user'=> $user,
            'confDatas'=>$confDatas,
            'tableList'=>$tableList,
            'builder'=>$builder,
            'name'=>$name]);
    }


    public function store(Request $request,String $name){
        // Получаем пользователя текущего
        $user=Auth::user();
        $datas=array();
        $confDatas=$this->getModel($name);
        $modelData = app($confDatas['models']);
        foreach ($confDatas['createColumn'] as $key=>$value){
            $datas[$key]=$request->input($key);
        }

        $insertData=$modelData::create(
            $datas
        );

        $modelLog=new LogCrud();
        $modelLog->user_id=$user->id;
        $modelLog->name=$name;
        $modelLog->event="new_record";
        $modelLog->new_data=json_encode($insertData);
        $modelLog->save();

        return redirect()->route('crud.list',['namecrud'=>$name]);
    }

}
