<?php

namespace Alsodev\Crudadmin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /**
     * Show page login
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function login(){
        return view('AlsodevCrudadmin::login');
    }


    public function home(){
        $user=Auth::user();
        return view("AlsodevCrudadmin::home",['user'=>$user]);
    }


    public function store(Request $request){
        $loginuser = $request->only('email', 'password');
        if (Auth::attempt($loginuser)) {
            return redirect()->route('crud.home');
        }
        return redirect()->route('login.page')->with('error', 'Ошибка авторизации. Пожалуйста, проверьте свой email и пароль.');
    }


    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        $prefix= config('alsodev_crudadmin.options.admin_puth');
        return redirect()->to($prefix.'/login');
    }
}
